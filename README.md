# Simple GPIO Breakout/Interposer for TEN64 boards

![Picture of GPIO Breakout installed](ten64-gpio-breakout.jpg)

This is a simple breakout PCB for the Ten64 which converts
the 26-pin, 1.27mm/0.050" pitch [GPIO/Control Header](https://ten64doc.traverse.com.au/hardware/control-header/)
to the more standard 2.54mm/0.100" pitch pins.

This is more convenient than using a ribbon cable for accessing
small numbers of GPIOs but __is not recommended for production__
as the connection to the host board is not keyed / constricted.

GPIOs are broken out into two rows of 13 pins for each side of the
board header.

**Warning:** The interface with the host board is not "keyed" and
shrouded (unlike an IDC ribbon cable) so it is very easy to misalign
the connector.

A good way to be sure is to check that the first and last pins on
both rows are connected to GND.

The interface with the board connector is effectively "friction-fit" so
no guarantees are made about it's ability to withstand vibration and
movement during transit.

**This board is not a Traverse Product and is not supported for
production use for reasons stated above**

You should also be mindful of the limitations of the GPIOs started
in the Ten64 manual - many of these pins come from the LS1088 SoC
and may not be forgiving against ESD, shorts and overvoltage
as a microcontroller.

## Schematic

![Schematic](Schematic.png)

## Bill of materials

The BOM is very simple:

| Designator | Quantity | Manufacturer | Manufacturer Part Number |
| ---------- | -------- | ------------ | ------------------------ |
| P1         | 1        | Samtec       | FLE-113-01-G-DV-A        |
| P2, P3     | 2        | Harwin       | M20-9991346              |

## Build your own

I have shared the project over at [pcbway](https://www.pcbway.com/project/shareproject/GPIO_Breakout_board_for_Traverse_Technologies_Ten64_appliance_fabbfc0b.html).
